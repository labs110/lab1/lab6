from numpy import arange
import data_labs

while True:

    For_G = list(map(float, input("Введите x_min, x_max, значение a и шаг для функции G: ").split()))
    For_F = list(map(float, input("Введите x_min, x_max, значение a и шаг для функции F: ").split()))
    For_Y = list(map(float, input("Введите x_min, x_max, значение a и шаг для функции Y: ").split()))

    log_data = [[], [], []]

    try:
        for i in arange(For_G[0], For_G[1], For_G[3]):
            
            # Блок расчёта и вывода данных G
            try:
                log_data[0].append(data_labs.g_function(i, For_G[2]))
            except ValueError:
                log_data[0].append(None)
        for i in arange(For_F[0], For_F[1], For_F[3]):
            
            # Блок расчёта и вывода данных F
            try:
                log_data[1].append(data_labs.f_function(i, For_F[2]))
            except ValueError or OverflowError:
                log_data[1].append(None)
        for i in arange(For_Y[0], For_Y[1], For_Y[3]):
            
            # Блок расчёта и вывода данных Y
            try:
                log_data[2].append(data_labs.y_function(i, For_Y[2]))
            except ValueError or OverflowError:
                log_data[2].append(None)
    except KeyboardInterrupt:
        print('Выполнение работы программы прекращено')

    [print(f"G: = {log_data[0][x]}") for x in range(len(log_data[0]))]
    [print(f"F: = {log_data[1][x]}") for x in range(len(log_data[1]))]
    [print(f"Y: = {log_data[2][x]}") for x in range(len(log_data[2]))]

    # Завершение/продолжение программы
    exit_choice = input("Начать выполнение программы снова? - (Д/Н)")
    if 'YyNnДдНн'.find(exit_choice) == -1:
        print('Ошибка выбора.')
    elif 'YyДд'.find(exit_choice) >= 0:
        pass
    elif 'NnНн'.find(exit_choice) >= 0:
        raise SystemExit()
